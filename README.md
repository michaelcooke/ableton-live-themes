# ableton-live-themes

This is a collection of themes that I've either created, modified, or just want to copy and hoard for potential future use.

## Contributing

I'm pretty nitpicky about themeing, but I encourage anybody who finds a color inconsistency or design bug to make a new issue to address it. Unfortunately, due to tight coupling of diseparate elements for themeing purposes, many if not all the themes uploaded here have many design compromises that present themselves as less than ideal.
